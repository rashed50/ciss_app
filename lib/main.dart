import 'package:ciss_app/core/Network/NetworkInfo.dart';
import 'package:ciss_app/presentation/Routes/AppRoutes.dart';
import 'package:ciss_app/presentation/Screen/BillCollection/PaymentScreenController.dart';
import 'package:ciss_app/presentation/Screen/Customer/AddCustomer/AddCustomerScreenController.dart';
import 'package:ciss_app/presentation/Screen/EmployeeInfo/NewEmployee/EmployeeInfoScreenController.dart';
import 'package:ciss_app/presentation/Screen/LogIn/LogInScreenController.dart';
import 'package:ciss_app/presentation/Screen/MyProfile/MyProfileScreenController.dart';
import 'package:ciss_app/presentation/Screen/Purchase/PurchaseScreenController.dart';
import 'package:ciss_app/presentation/Screen/Sales/SalesScreenController.dart';
import 'package:ciss_app/presentation/Screen/SubContact/SubContactScreenController.dart';
import 'package:ciss_app/presentation/Screen/Vendor/VendorScreenController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'data/Repository/Remote/CompanyProfileRemoteRepository.dart';
import 'presentation/Screen/Customer/CustomerScreenController.dart';
import 'presentation/Screen/HomePage/HomeScreenController.dart';
import 'presentation/Screen/Income/IncomeScreenController.dart';
import 'presentation/Screen/LogOut/LogOutScreenController.dart';




void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => NetworkInfoController());
    Get.put(HomeScreenController(),);
    Get.put(CustomerScreenController(),);
    Get.put( IncomeScreenController(),);
    Get.put( LogOutScreenController(),);
    Get.put( MyProfileScreenController(),);
    Get.put( PaymentScreenController(),);
    Get.put( PurchaseScreenController(),);
    Get.put( SalesScreenController(),);
    Get.put( SubContactScreenController(),);
    Get.put( VendorScreenController(),);
    Get.put(LogInScreenController(),);
    Get.put(EmployeeScreenController(),);
    Get.put(AddCustomerScreenController(),);
    Get.put(CompanyProfileRemoteRepository(),);
  //  Get.put(CompanyProfileRepository());
   // Get.put(NetworkInfoController());
    //Get.put( ,);
    return GetMaterialApp(
      getPages: AppRoutes.appRoutesList(),
      initialRoute: AppRoutes.LOGIN_SCREEN,
    );
  }
}
