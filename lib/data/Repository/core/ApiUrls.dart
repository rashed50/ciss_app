

class ApiUrls{
 // static String base_url = "http://ipmsappapi.starhairbd.com/api/";
  static String base_url = "https://netbill.saaddiagnostic.com/api/";
    //  "https://ciss.3iengineers.com/api/";
  //Authentication
  static String get_company_profile = "${base_url}get-company-profile";

  // Company Asset information
  static String save_Company_Asset_Information = "${base_url}save-assetinfo";
  static String add_new_customer = "${base_url}add-new-customer";

  static String get_division = "${base_url}get-divisions";
  static String get_district = "${base_url}get-districts";
  static String get_upazilla = "${base_url}get-upazilas";
  static String get_union = "${base_url}get-packages";


    static String save_serivce_package = "${base_url}save-package";
    static String get_serivce_package = "${base_url}get-packages";
}