
import 'package:ciss_app/data/Model/CustomerModel.dart';
import 'package:ciss_app/data/Repository/Error/failures.dart';
import 'package:ciss_app/data/Repository/Remote/CustomerRemoteRepository.dart';
import 'package:ciss_app/data/Repository/core/AppAPIKey.dart';
import 'package:dartz/dartz.dart';

class CustomerRepository{

 static Future<bool> saveNewCustomerInformation(CustomerModel employeeModel) async{

    Either<dynamic,Failure> apiResponse = await CustomerRemoteRepository.insertNewCustomerInformation(employeeModel.toJson());
    apiResponse.fold((l){
      if (l[AppAPIKey.success].toString() == "true" && l[AppAPIKey.status_code].toString() == '200' ){
        return true;
      }
    }, (r){

    });

    return false;
  }

}