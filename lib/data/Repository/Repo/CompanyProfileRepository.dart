import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:get/get.dart';
import 'package:ciss_app/data/Model/AssetInfoModel.dart';
import 'package:ciss_app/data/Model/CompanyProfileModel.dart';
import 'package:ciss_app/data/Repository/Error/failures.dart';
import 'package:ciss_app/data/Repository/Remote/CompanyProfileRemoteRepository.dart';


class CompanyProfileRepository{

 Future<CompanyProfileModel> getCompanyProfileInformation() async{

    CompanyProfileRemoteRepository  remote = new CompanyProfileRemoteRepository();
    Either<dynamic, Failure> response = await remote.getCompanyProfileInformation();
    response.fold((l) {
      Map<String,dynamic> data = jsonDecode(l.body);
        print(data['message']);
        if(data['status_code'] == 200) {
          CompanyProfileModel companyProfileModel = CompanyProfileModel
              .fromJson(data['message'][0]);

          return companyProfileModel;
        }
    }, (r) {
      print('error occured');
      return null;
    });


  }


 Future<bool> saveCompanyAssetInformation(AssetInfoModel assetInfo) async{
   CompanyProfileRemoteRepository empRemoteRepo = new CompanyProfileRemoteRepository();
   Either<dynamic,Failure> apiResponse = await empRemoteRepo.insertCompanyAssetInformation(assetInfo.toJson());
   apiResponse.fold((l){
     Map<String,dynamic> jsonresponse = jsonDecode(l.body);
     print(jsonresponse);
     if(jsonresponse['status_code'] == 200){
       return true;
     }
   }, (r){

   });

   return false;
 }


 Future<Either<dynamic,Failure>> getDivisions() async{

   CompanyProfileRemoteRepository packageRemoteRepo = new CompanyProfileRemoteRepository();
   Either<dynamic, Failure> response = await packageRemoteRepo.getDivisions();
   response.fold((l) {
     response = Left(l);
   }, (r) {
     response = Right(r);
   });
   return response;


 }

 Future<Either<dynamic,Failure>> getDistricts(int divisionId) async{

   CompanyProfileRemoteRepository packageRemoteRepo = new CompanyProfileRemoteRepository();
   Either<dynamic, Failure> response = await packageRemoteRepo.getDistricts(divisionId);
   response.fold((l) {
     response = Left(l);
   }, (r) {
     response = Right(r);
   });
   return response;


 }


 Future<Either<dynamic,Failure>> getUpazillas(int districtId) async{

   CompanyProfileRemoteRepository packageRemoteRepo = new CompanyProfileRemoteRepository();
   Either<dynamic, Failure> response = await packageRemoteRepo.getUpazilla(districtId);
   response.fold((l) {
     response = Left(l);
   }, (r) {
     response = Right(r);
   });
   return response;


 }


 Future<Either<dynamic,Failure>> getUnions(int upazillaId) async{

   CompanyProfileRemoteRepository packageRemoteRepo = new CompanyProfileRemoteRepository();
   Either<dynamic, Failure> response = await packageRemoteRepo.getUnions(upazillaId);
   response.fold((l) {
     response = Left(l);
   }, (r) {
     response = Right(r);
   });
   return response;


 }




}