
import 'dart:convert';

import 'package:ciss_app/data/Model/PackageModel.dart';
import 'package:ciss_app/data/Repository/Error/failures.dart';
import 'package:ciss_app/data/Repository/Remote/PackageRemoteRepository.dart';
import 'package:ciss_app/data/Repository/core/AppAPIKey.dart';
import 'package:dartz/dartz.dart';
import 'package:dartz/dartz_unsafe.dart';
import 'package:get/get.dart';

class PackageRepository {


  static Future<bool> saveNewPackageInformation(PackageModel packageModel) async{
    PackageRemoteRepository packageRemoteRepo = new PackageRemoteRepository();

    Either<dynamic,Failure> apiResponse = await packageRemoteRepo.saveNewPackageInformation(packageModel.toJson());
    apiResponse.fold((l){
     // Map<String,dynamic> jsonresponse = jsonDecode(l.body);
      if(l['status_code'].toString() == '200'){
        print('new package ========= data saved ');
        return true;
      }else {
        print('package api error ${apiResponse}');
      }
    }, (r){
      print('new package data not saved ');
    });
    print('package api response ${apiResponse}');
    return false;
  }

  static  Future<Either<dynamic, Failure>> getServicePackageInformation() async{

    PackageRemoteRepository packageRemoteRepo = new PackageRemoteRepository();
   // Either<Response, Failure>
    Either<dynamic, Failure> response = await packageRemoteRepo.getServicePackages(1);
    response.fold((l) {
      response = Left(l);
    }, (r) {
      response = Right(r);
    });
    return response;


  //   if (l[AppAPIKey.success].toString() == 'true' && l[AppAPIKey.status_code].toString() == '200' )
  //   {
  //
  //         List<PackageModel> packageModels = List<PackageModel>();
  //
  //         print("@/n Package loaded ===== ${l['data']}");
  //      // packageModels =  l['data'].map((data) => PackageModel.fromJson(data)).toList<PackageModel>();
  //         l['data'].forEach((v) {
  //           packageModels.add(PackageModel.fromJson(v));
  //          });
  //      // print(packageModels);
  //       return packageModels;
  //   }
  //   else
  //   {
  //     print("Package not loaded ===== ");
  //     //dataLoading.value = true;
  //     // AppSnackBar.errorSnackbar(msg: l['message']);
  //   }
  // }, (r) {
  // print("Right ${r}");
  // return null;
  // });


  }


}