

import 'dart:convert';

import 'package:dartz/dartz.dart';

import '../Error/failures.dart';
import '../core/ApiClient.dart';
import '../core/ApiUrls.dart';

class CompanyProfileRemoteRepository{

  Future<Either<dynamic,Failure>> getCompanyProfileInformation() async{
    try{
     // if(await NetworkInfoController.to.isConnected)
     //  {
        print('Phone no of the employee is ');
        dynamic response = await ApiClient.Request(
            url: ApiUrls.get_company_profile, //ApiUrls.phone_no_verificaton,//
            enableHeader: false,
            body: 'jhhj',
            method: Method.GET
        );
     return Left(response);
    //  }

    }
    catch(err) {
      print("Error : ${err}");
      return Right(ServerFailure());
    }
  }

  Future<Either<dynamic,Failure>> insertCompanyAssetInformation(Map<String,dynamic> json) async{
    try{
      // if(await NetworkInfoController.to.isConnected)
      //  {
      print('Phone no of the employee is ');
      dynamic response = await ApiClient.Request(
          url: ApiUrls.save_Company_Asset_Information,
          enableHeader: false,
          body: jsonEncode(json),
          method: Method.POST
      );
      return Left(response);
      //  }

    }
    catch(err) {
      print("Error : ${err}");
      return Right(ServerFailure());
    }
  }


  //
 // Get Division , District , Upozilla, Union
 //
  Future<Either<dynamic, Failure>> getDivisions() async {
    try {
      Map<String, dynamic> bodyData = {
        "id": 1,
      };
      // if (await _networkInfoController.isConnected) {
      dynamic response = await ApiClient.Request(
          url: ApiUrls.get_division,
          body: jsonEncode(bodyData),
          enableHeader: true,
          method: Method.GET);
      print( "package details Response ::: ${response}");
      return Left(response);
      //}
      //else {
      //   print("Noooooooooo Network");
      //   return Right(NoConnectionFailure());
      // }
    } catch (err) {
      print("Error : ${err} ");
      return Right(ServerFailure());
    }
  }
  Future<Either<dynamic, Failure>> getDistricts(int divisionId) async {
    try {
      Map<String, dynamic> bodyData = {
        "id": divisionId,
      };
      // if (await _networkInfoController.isConnected) {
      dynamic response = await ApiClient.Request(
          url: ApiUrls.get_district,
          body: jsonEncode(bodyData),
          enableHeader: true,
          method: Method.GET);
      print( "package details Response ::: ${response}");
      return Left(response);
      //}
      //else {
      //   print("Noooooooooo Network");
      //   return Right(NoConnectionFailure());
      // }
    } catch (err) {
      print("Error : ${err} ");
      return Right(ServerFailure());
    }
  }
  Future<Either<dynamic, Failure>> getUpazilla(districtId) async {
    try {
      Map<String, dynamic> bodyData = {
        "id": districtId,
      };
      // if (await _networkInfoController.isConnected) {
      dynamic response = await ApiClient.Request(
          url: ApiUrls.get_upazilla,
          body: jsonEncode(bodyData),
          enableHeader: true,
          method: Method.GET);
      print( "package details Response ::: ${response}");
      return Left(response);
      //}
      //else {
      //   print("Noooooooooo Network");
      //   return Right(NoConnectionFailure());
      // }
    } catch (err) {
      print("Error : ${err} ");
      return Right(ServerFailure());
    }
  }
  Future<Either<dynamic, Failure>> getUnions(upazillaId) async {
    try {
      Map<String, dynamic> bodyData = {
        "id": upazillaId,
      };
      // if (await _networkInfoController.isConnected) {
      dynamic response = await ApiClient.Request(
          url: ApiUrls.get_union,
          body: jsonEncode(bodyData),
          enableHeader: true,
          method: Method.GET);
      print( "package details Response ::: ${response}");
      return Left(response);
      //}
      //else {
      //   print("Noooooooooo Network");
      //   return Right(NoConnectionFailure());
      // }
    } catch (err) {
      print("Error : ${err} ");
      return Right(ServerFailure());
    }
  }
}