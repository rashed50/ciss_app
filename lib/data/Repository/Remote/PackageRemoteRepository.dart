import 'dart:convert';

import 'package:ciss_app/core/Network/NetworkInfo.dart';
import 'package:ciss_app/data/Repository/Error/failures.dart';
import 'package:ciss_app/data/Repository/core/ApiClient.dart';
import 'package:ciss_app/data/Repository/core/ApiUrls.dart';
import 'package:dartz/dartz.dart';

class PackageRemoteRepository  {

 Future<Either<dynamic,Failure>> saveNewPackageInformation(Map<String,dynamic> json) async{
    try{
       //if(await NetworkInfoController.to.isConnected)
     //   {

      dynamic response = await ApiClient.Request(
          url: ApiUrls.save_serivce_package,
          enableHeader: false,
          body: json,
          method: Method.POST
      );
      print('save new package ');
      return Left(response);
       //  }
       //  else {
       //   return Right(NoConnectionFailure());
       // }

    }
    catch(err) {
      print("Error : ${err}");
      return Right(ServerFailure());
    }
  }



 Future<Either<dynamic, Failure>> getServicePackages(int orderId) async {
   try {
     Map<String, dynamic> bodyData = {
       "id": orderId,
     };
    // if (await _networkInfoController.isConnected) {
       dynamic response = await ApiClient.Request(
           url: ApiUrls.get_serivce_package,
           body: jsonEncode(bodyData),
           enableHeader: true,
           method: Method.GET);
       print( "package details Response ::: ${response}");
       return Left(response);
      //}
      //else {
     //   print("Noooooooooo Network");
     //   return Right(NoConnectionFailure());
     // }
   } catch (err) {
     print("Error : ${err} ");
     return Right(ServerFailure());
   }
 }

}