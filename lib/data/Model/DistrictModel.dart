class DistrictModel {
  var districtId,districtName;

  DistrictModel({this.districtId,this.districtName});
  DistrictModel.fromJSon(Map<String,dynamic>json){
    this.districtId = json['districtId'];
    this.districtName = json['districtName'];
  }
}