

class AssetInfoModel {


  String AssetName, decription;
  int assetId,assetTypeId,entryById,lifeTime,depriciationValue;
  double faceValue;
  DateTime entryDate;

  AssetInfoModel.fromJson(Map<String, dynamic> json) {
    print('calling for making model '+json['AssetName']);
    this.assetId = json['assetId'];
    this.assetTypeId = json['assetTypeId'];
    this.entryById = json['entryById'];
    this.AssetName = json['AssetName'];
    this.faceValue = json['faceValue'];
    this.lifeTime = json['lifeTime'];
    this.depriciationValue = json['depriciationValue'];
    this.decription = json['decription'];
    this.entryDate = json['entryDate'];
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
   // data['assetId'] = this.assetId.toString();
    data['assetTypeId'] = this.assetTypeId.toString();
    data['entryById'] = this.entryById.toString();
    data['AssetName'] = this.AssetName;
    data['faceValue'] = this.faceValue.toString();
    data['lifeTime'] = this.lifeTime.toString();
    data['depriciationValue'] = this.depriciationValue.toString();
    data['decription'] = this.decription;
    data['entryDate'] = this.entryDate.toString();


    return data;
  }



   AssetInfoModel();
}

