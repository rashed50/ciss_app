class ServiceTypeModel {
  var serviceTypeId,serviceTypeName;

  ServiceTypeModel({this.serviceTypeId,this.serviceTypeName});
  ServiceTypeModel.fromJSon(Map<String,dynamic>json){
    this.serviceTypeId = json['serviceTypeId'];
    this.serviceTypeName = json['serviceTypeName'];
  }
}