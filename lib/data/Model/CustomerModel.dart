import 'dart:core';

class CustomerModel{
  String  autoId,customerId,customerName,fatherName,email,applicationDate,phoneNo1,phoneNo2,connectionDate,connectionStatusId,
      customerOccupationId, description, postCode,roadNo,houseNo,floorNo,
      customerPhoto,nid;
  int countryId, divisionId,districtId,upazilaId,unionId,professionId,serviceTypeId,packageId,serviceAreaId,serviceSubAreaId,insertedById;


  CustomerModel.fromJson(Map<String, dynamic> json) {
    print('calling for making model ' + json['customerName']);
    this. customerName = json['customerName'];
    this.fatherName = json['fatherName'];
    this.email = json['email'];
    this.applicationDate = json['applicationDate'];
    this.phoneNo1 = json['phoneNo1'];
    this.phoneNo2 = json['employeeMobileNo2'];
    this.connectionDate = json['employeePhoto'];
    this.connectionStatusId = json['connectionStatusId'];
    this.customerOccupationId = json['customerOccupationId'];
    this.serviceTypeId = json['serviceTypeId'];
    this.packageId = json['packageId'];
   // this.countryId = json['countryId'];
    this.divisionId = json['divisionId'];
    this.districtId = json['districtId'];
    this.upazilaId = json['upazilaId'];
    this.unionId = json['unionId'];
    this.serviceAreaId = json['serviceAreaId'];
    this.serviceSubAreaId = json['serviceSubAreaId'];
    this.professionId = json['professionId'];
    this.description = json['description'];
    this.postCode = json['postCode'];
    this.roadNo = json['roadNo'];
    this.houseNo = json['houseNo'];
    this.floorNo = json['floorNo'];
    this.nid = json['nid'];




  }



  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['customerName'] = this.customerName;
    data['fatherName'] = this.fatherName;
    data['email'] = this.email;
    data['applicationDate'] = this.applicationDate;
    data['phoneNo1'] = this.phoneNo1;
    data['phoneNo2'] = this.phoneNo2;
    data['connectionDate'] = this.connectionDate;
    data['connectionStatusId'] = this.connectionStatusId;
    data['customerOccupationId'] = this.customerOccupationId;
    data['serviceTypeId'] = this.serviceTypeId.toString();
    data['packageId'] = this.packageId.toString();
    data['divisionId'] = this.divisionId.toString();
    data['districtId'] = this.districtId.toString();
    data['upazilaId'] = this.upazilaId.toString();
    data['unionId'] = this.unionId.toString();
    data['serviceAreaId'] = this.serviceAreaId.toString();
    data['serviceSubAreaId'] = this.serviceSubAreaId.toString();
    data['professionId'] = this.professionId.toString();
    data['postCode'] = this.postCode.toString();
    data['roadNo'] = this.roadNo.toString();
    data['houseNo'] = this.houseNo.toString();
    data['floorNo'] = this.floorNo.toString();
    data['nid'] = this.nid.toString();
    data['plateNo'] = this.floorNo.toString();


    return data;
  }
}




