class UnionModel {
  var unionId,unionName;

  UnionModel({this.unionId,this.unionName});
  UnionModel.fromJSon(Map<String,dynamic>json){
    this.unionId = json['unionId'];
    this.unionName = json['unionName'];
  }
}