import 'package:ciss_app/data/Model/ServiceTypeModel.dart';

class PackageModel {
  var packageId, packageName,bandWidth,price,packageCode;
  ServiceTypeModel serviceTypeModel;

  PackageModel({this.packageName,this.packageCode,this.bandWidth,this.price});

  PackageModel.fromJson(Map<String,dynamic> json){
    this.packageId = json['packageId'];
    this.packageName = json['packageName'];
    this.bandWidth = json['bandwidth'];
    this.price = json['price'];
    this.packageCode = json['packageCode'];
    print(json['packageId']);
   // this.serviceTypeModel = ServiceTypeModel.fromJSon(json);

  }



  Map<String, String> toJson() {
    final Map<String, dynamic> data = new Map<String, String>();
    data['packageName'] = this.packageName;
    data['bandwidth'] = this.bandWidth;
    data['price'] = this.price;
    data['packageCode'] = this.packageCode;
    data['serviceTypeId'] = this.serviceTypeModel.serviceTypeId;
    return data;
  }
}