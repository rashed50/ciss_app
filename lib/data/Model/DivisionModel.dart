class DivisionModel {
  var divisionName,divisionId;

  DivisionModel({this.divisionId,this.divisionName});
  DivisionModel.fromJSon(Map<String,dynamic>json){
    this.divisionId = json['divisionId'];
    this.divisionName = json['divisionName'];
  }
}