import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
 class AppColors{
    static Color cursorColor = Colors.black;
    static  Color appBarColor = Colors.blue;
    static  Color appBarTitleColor = Colors.white;
    static Color bodyTextColor = Colors.black;
    static Color buttonBGColor = const Color(0xaa0bb0aa);// Colors.pinkAccent;
    static Color buttonBorderColor = const Color(0xee0bb0ff);
    static Color buttonSplashColor = Colors.deepOrange;
    static Color buttonIconColor = Colors.white;
    static Color bodyBackgroundColor = const Color(0xffe0e0e0);
    static Color bottomNevigationBarColor = Colors.blue;
    static Color blackColor = Colors.black;
 }


