
import 'package:ciss_app/data/Model/ServiceTypeModel.dart';

 class PredefinedDataMaker{

  static List<ServiceTypeModel> getServiceTypeData(){
   List<ServiceTypeModel> serviceTypes  = List<ServiceTypeModel>();
   ServiceTypeModel aserviceType = new ServiceTypeModel();
    aserviceType.serviceTypeId = 1;
    aserviceType.serviceTypeName = 'Internet Service';
    serviceTypes.add(aserviceType);
    aserviceType = new ServiceTypeModel();
    aserviceType.serviceTypeId = 2;
    aserviceType.serviceTypeName = 'Dish Service';
    serviceTypes.add(aserviceType);
   return serviceTypes;
  }


  static List<String> getCustomerProfesion(){
   List<String> profesions  = List<String>();

    profesions.add('Private Job');
    profesions.add('Public Job');
    profesions.add('Business');
   return profesions;
  }

  static int getCustomerProfessionId(String professionName){

  if(professionName ==  'Private Job')
   return 1;
  else  if(professionName ==  'Public Job')
   return 2;
   else  if(professionName ==  'Business')
   return 3;
   else
    return -1;
  }

}

