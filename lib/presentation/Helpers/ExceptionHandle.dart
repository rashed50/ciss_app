

import 'package:ciss_app/data/Repository/Error/failures.dart';
import 'package:ciss_app/presentation/widgets/AppSnackBar.dart';

class ExceptionHandle {
  static exceptionHandle(Failure failure) {
    if (failure is ServerFailure) {
      AppSnackBar.errorSnackbar(msg: "Server Exception }");
    } else if (failure is NoConnectionFailure) {
      AppSnackBar.errorSnackbar(msg: "Please check your internet connection");
    }
  }
}
