import 'package:ciss_app/presentation/widgets/AppColors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ciss_app/presentation/Routes/AppRoutes.dart';
class EmployeeMenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.bodyBackgroundColor,
      height: Get.height,
      width: Get.width,
      child:  SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                RaisedButton.icon(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                  onPressed: (){
                    Get.toNamed(AppRoutes.ADDCUSTOMER_SCREEN);
                  },
                  elevation: 15,
                  color: Colors.blue,
                  splashColor: Colors.red,
                  focusColor: Colors.deepOrange,
                  icon: Icon(Icons.add,color: Colors.white,),
                  label: Text('Add Employee',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),),
                RaisedButton.icon(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                  onPressed: (){
                  },
                  elevation: 15,
                  color: Colors.blue,
                  splashColor: Colors.red,
                  icon: Icon(Icons.list_alt,color: Colors.white,),
                  label: Text('Employee List',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),),
              ],
            ),
          ),
        ),
      ),

    );
  }
}
