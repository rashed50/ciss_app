import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ciss_app/presentation/Routes/AppRoutes.dart';
import 'package:ciss_app/presentation/Screen/BillCollection/PaymentScreen.dart';
import 'package:ciss_app/presentation/Screen/Customer/CustomerScreen.dart';
import 'package:ciss_app/presentation/Screen/Drawer/DrawerMenu.dart';
import 'package:ciss_app/presentation/Screen/EmployeeInfo/EmployeeMenu/EmployeeMenuScreen.dart';
import 'package:ciss_app/presentation/Screen/EmployeeInfo/NewEmployee/EmployeeInfoScreen.dart';
import 'package:ciss_app/presentation/Screen/Expenditure/ExpenditureScreen.dart';
import 'package:ciss_app/presentation/Screen/HomePage/HomeScreenController.dart';
import 'package:ciss_app/presentation/Screen/Income/IncomeScreen.dart';
import 'package:ciss_app/presentation/Screen/LogOut/LogOutScreen.dart';
import 'package:ciss_app/presentation/Screen/MyProfile/MyProfileScreen.dart';
import 'package:ciss_app/presentation/Screen/PackageInfo/ServicePackageScreen.dart';
import 'package:ciss_app/presentation/Screen/Purchase/PurchaseScreen.dart';
import 'package:ciss_app/presentation/Screen/Sales/SalesScreen.dart';
import 'package:ciss_app/presentation/Screen/ServiceArea/ServiceAreaScreen.dart';
import 'package:ciss_app/presentation/Screen/SubContact/SubContactScreen.dart';
import 'package:ciss_app/presentation/Screen/Vendor/VendorScreen.dart';
class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title:Text('CISS'),
      ),


      drawer: DrawerMenu(),
      body: Container(
        child: GetBuilder<HomeScreenController>(
          builder: (obj){
            if( obj.screeenId == 1){
              return MyProfileScreen();
            }
            if( obj.screeenId == 2){
              return ServiceAreaScreen();
            }
            if( obj.screeenId == 3){
              return ServicePackageScreen();
            }
            if( obj.screeenId == 4){
              return EmployeeMenuScreen();
            }
            if( obj.screeenId == 5){
              return CustomerScreen();
            }
            if( obj.screeenId == 6){
              return PaymentScreen();
            }
            if( obj.screeenId == 7){
              return ExpenditureScreen();
            }
            if( obj.screeenId == 8){
              return IncomeScreen();
            }
            if( obj.screeenId == 9){
              return LogOutScreen();
            }
             return SafeArea(child: Container(
               height: Get.height,
               width: Get.width,
               color: Colors.blue,
               child: Text('Home'),
             )
             ) ;
          }
        ),
      ),

    );
  }
}
