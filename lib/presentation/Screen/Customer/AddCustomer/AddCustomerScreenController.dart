import 'package:ciss_app/data/Model/CustomerModel.dart';
import 'package:ciss_app/data/Model/PackageModel.dart';
import 'package:ciss_app/data/Model/ServiceTypeModel.dart';
import 'package:ciss_app/data/Repository/Error/failures.dart';
import 'package:ciss_app/data/Repository/Remote/CustomerRemoteRepository.dart';
import 'package:ciss_app/data/Repository/Repo/CustomerRepository.dart';
import 'package:ciss_app/data/Repository/Repo/PackageRepository.dart';
import 'package:ciss_app/data/Repository/core/AppAPIKey.dart';
import 'package:ciss_app/presentation/Helpers/PredefinedDataMaker.dart';
import 'package:ciss_app/presentation/widgets/AppColors.dart';
import 'package:ciss_app/presentation/widgets/AppSnackBar.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddCustomerScreenController extends GetxController{
  static AddCustomerScreenController to = Get.find();
  TextEditingController customerNameTxt = new TextEditingController();
  TextEditingController customerFatherNameTxt = new TextEditingController();
  TextEditingController customerEmailTxt = new TextEditingController();
  TextEditingController customerPhoneNoTxt = new TextEditingController();
  TextEditingController addressDetails = new TextEditingController();
  TextEditingController flatNoTxt = new TextEditingController();
  TextEditingController postCodeTxt = new TextEditingController();
  TextEditingController houseNoTxt = new TextEditingController();
  TextEditingController roadNoTxt = new TextEditingController();

  List<ServiceTypeModel> serviceTypes  = List<ServiceTypeModel>();
 // ServiceTypeModel aserviceType  = new ServiceTypeModel();
 List<String> profesions = List<String>();
 String selectProfesion = null;
 RxString selectedDate = "Select Connection Date".obs;

  RxList<PackageModel> packageList = <PackageModel>[].obs;
  int selectedServiceId,selectedPackageId,selectedProfessionId,divisionId,districtId,upazillaId,unionId,selectedServiceAreaId ;
  RxList<DropdownMenuItem<PackageModel>> _dropdownMenuItems = <DropdownMenuItem<PackageModel>>[].obs;


  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }


  /*
   =============================================================================
   ============================= New Case Save =================================
   =============================================================================
   */

  saveNewCustomerInformation() async {
   // loading(true);
    try {

      if (NewCustomerFormValidation()) {
       // print("from validation ok  ===== ");

        CustomerModel acustomer;// = new CustomerModel();
        acustomer. customerName = customerNameTxt.text;
        acustomer.fatherName = customerFatherNameTxt.text;
        acustomer.email = customerEmailTxt.text;
        acustomer.applicationDate = selectedDate.toString();
        acustomer.phoneNo1 = customerPhoneNoTxt.text;
        acustomer.phoneNo2 =  customerPhoneNoTxt.text;
        acustomer.connectionDate =  selectedDate.toString();
        acustomer.connectionStatusId = 1.toString();
        acustomer.customerOccupationId = selectedProfessionId.toString();
        acustomer.serviceTypeId = selectedServiceId;
        acustomer.packageId = selectedPackageId;
        acustomer.divisionId = divisionId;
        acustomer.districtId = districtId;
        acustomer.upazilaId = upazillaId;
        acustomer.unionId = unionId;
        acustomer.serviceAreaId = 1;
        acustomer.serviceSubAreaId = 1;
        acustomer.professionId = selectedProfessionId;
        acustomer.description = "";
        acustomer.postCode = postCodeTxt.text;
        acustomer.roadNo = roadNoTxt.text;
        acustomer.houseNo = houseNoTxt.text;
        acustomer.floorNo = flatNoTxt.text;
        acustomer.floorNo = flatNoTxt.text;
        acustomer.nid = "";

        bool isSuccess  = await CustomerRepository.saveNewCustomerInformation(acustomer);


          if(isSuccess){
            print("case save  ===== ");
            AppSnackBar.successSnackbar(msg:"Case Save Successfully Completed");
          }
          else
          {
            print("case save  ===== ");
           // loading(false);
           // AppSnackBar.errorSnackbar(msg: l['message']);
            messageSnackbar(msg: 'Failed ', title: "Error!");
          }




      } else {
       // loading(false);
      }

    } catch (err) {
      printInfo(info: "Error : ${err}");
      AppSnackBar.errorSnackbar(msg: "");
     // loading(false);
    }
  }
  // getAllCaseInformation()async {
  //
  //   dataLoading.value = true;
  //   Either<dynamic, Failure> response = await _caseInformationAPI.getCaseMasterLog();
  //   response.fold((l) {
  //
  //     if (l[APIKey.status].toString() ==APIKey.success && l[APIKey.status_code].toString() == '200' )
  //     {
  //       caseMasterLogList.value = (l[APIKey.data] as List).map((item) => CaseModel.fromJSON(item)).toList();
  //       dataLoading.value = false;
  //       print("case type loaded ===== ${caseMasterLogList.length}");
  //       if(caseMasterLogList.length == 0){
  //         AppSnackBar.errorSnackbar(msg: "Data not found");
  //       }
  //     }
  //     else
  //     {
  //       print("case type not loaded ===== ");
  //       dataLoading.value = false;
  //       AppSnackBar.errorSnackbar(msg: "Data Loading Error");
  //     }
  //   }, (r) {
  //     print("Right ${r}");
  //   });
  //
  //
  // }

  NewCustomerFormValidation() {
    if (customerNameTxt.text.isEmpty) {
      messageSnackbar(msg: "Case Name is required", title: "Error!");
      return false;
    }
    if (customerFatherNameTxt.text.isEmpty) {
      customerFatherNameTxt.text = "";
    }

    if (customerEmailTxt.text.isEmpty) {
      customerEmailTxt.text = "";
    }
    if (customerPhoneNoTxt.text.isEmpty) {
      messageSnackbar(msg: "Mobile Number is required", title: "Error!");
      return false;
    }
    if (divisionId <=  0) {
       divisionId = 1;
    }
     if (districtId <=  0) {
      messageSnackbar(msg: "Select District Name", title: "Error!");
      return false;
     }
    if ( upazillaId <=  0) {
      messageSnackbar(msg: "Select Upazilla Name", title: "Error!");
      return false;
    }
    if ( unionId <=  0) {
      // messageSnackbar(msg: "Select Upazilla Name", title: "Error!");
      // return false;
      unionId = 1;
    }

    if (postCodeTxt.text.isEmpty) {
       postCodeTxt.text = " ";
    }
    if (roadNoTxt.text.isEmpty) {
      roadNoTxt.text = " ";
    }
    if (houseNoTxt.text.isEmpty) {
      houseNoTxt.text = " ";
    }
    if (flatNoTxt.text.isEmpty) {
      flatNoTxt.text = " ";
    }

    selectedProfessionId = PredefinedDataMaker.getCustomerProfessionId(selectProfesion);

    if (selectedProfessionId <=  0) {
      selectedProfessionId = 1;
    }

    if (selectedServiceId <=  0) {
      messageSnackbar(msg: "Choose Service Name", title: "Error!");
      return false;
    }

    if (selectedPackageId <=  0) {
      messageSnackbar(msg: "Choose a Package", title: "Error!");
      return false;
    }

    if (selectedDate.isEmpty) {
      selectedDate.value = DateTime.now().toString();
    }

    if (selectedServiceAreaId <=  0) {
     // messageSnackbar(msg: "Choose a Package", title: "Error!");
     // return false;
      selectedServiceAreaId = 1;
    }
    return true;
  }
  void messageSnackbar({@required String msg, @required String title}) {
    return Get.snackbar('$msg', title,
        snackPosition: SnackPosition.TOP,
        backgroundColor: AppColors.appBarColor,
        colorText: AppColors.blackColor);
  }

  /*
   =============================================================================
   ============================= Initial Screen Loading Data====================
   =============================================================================
   */

  loadServiceTypeAPIData() {
    serviceTypes =   PredefinedDataMaker.getServiceTypeData();
    profesions = PredefinedDataMaker.getCustomerProfesion();
     print('Service TYpe loaded ');
     loadALlInitialFormData();
  }
  loadALlInitialFormData(){
   loadAllPackages();
  }
  loadAllPackages() async{
      Either<dynamic, Failure> response = await PackageRepository.getServicePackageInformation();

      response.fold((l) {

        if(l== null)
          return;
        print("Left ===>  ${l}");
        if (l[AppAPIKey.success].toString() == 'true' && l[AppAPIKey.status_code].toString() == '200' ) {

          List<PackageModel> lst = (l['data'] as List).map((itemWord) => PackageModel.fromJson(itemWord)).toList();
          packageList.value = lst;
          packageList.refresh();
          print('all completed orders ${packageList}');
        }else {
          // AppSnackBar.errorSnackbar(msg: l['message']);
        }
      }, (r) {
        print("Right ${r}");
      });


    }

  loadFormInitialData(){
    serviceTypes = PredefinedDataMaker.getServiceTypeData();
    print('Service TYpe loaded ');
  }






}