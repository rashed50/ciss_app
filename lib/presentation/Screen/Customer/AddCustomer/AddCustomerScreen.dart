import 'package:ciss_app/data/Model/PackageModel.dart';
import 'package:ciss_app/presentation/Screen/Customer/AddCustomer/AddCustomerScreenController.dart';
import 'package:ciss_app/presentation/widgets/AppButtonWidget.dart';
import 'package:ciss_app/presentation/widgets/AppColors.dart';
import 'package:ciss_app/presentation/widgets/AppTextFieldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:get/get.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_image_picker/form_builder_image_picker.dart';
class AddCustomerScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();

  List<String> CountryOptions = ['Bangladesh','India','Pakisthan','Sri-Lanka','Englend'];
  List<String> divisions = ['Dhaka','Chattrogram','Borishal','Sylhet','Khulna'];
  List<String> districts = ['Dhaka Sadar','Gazipur','Manikganj','Khula Sadar','Rajbari'];
  List<String> upazillas = ['Dhaka Sadar','Savar','Gazipur Sadar','Chuadanga','Rajbari Sadar'];
  @override
  Widget build(BuildContext context) {
    AddCustomerScreenController.to.loadServiceTypeAPIData();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.appBarColor,
        title: Text('ADD CUSTOMER',
        style: TextStyle(
          fontSize: 20,
          color: AppColors.appBarTitleColor,
          fontWeight: FontWeight.w800
        ),
        ),
        centerTitle: true,
      ),
      bottomNavigationBar: Container(
        color: AppColors.bottomNevigationBarColor,
        height: 25,
        width: Get.width,
      ),
      backgroundColor:AppColors.bodyBackgroundColor,
      body: FormBuilder(
        key: _formKey,
        child: SafeArea(
          child:SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:<Widget> [
                SizedBox(height: 20,),
                Center(
                  child: Text('New Customer Information',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.w800,
                      letterSpacing: 0,
                    ),),
                ), // form title
                SizedBox(height: 15,),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Column(
                    children: <Widget>[


    //               $table->string('customerName');
    //                 $table->string('fatherName')->nullable();;
    //         $table->string('email');
    //       $table->string('applicationDate');
    //     $table->string('phoneNo1');
    // $table->string('phoneNo2')->nullable();
    // $table->string('connectionDate');
    // $table->integer('connectionStatusId')->default(0);
    // $table->integer('cusmerOccupationId')->default(0);
    // $table->integer('serviceTypeId');
    // $table->integer('servicePackageId');
    // $table->integer('divisionId')->default(1);
    // $table->integer('districtId');
    // $table->integer('upazilaId');
    // $table->integer('serviceAreaId')->default(0);
    // $table->integer('serviceSubAreaId')->default(0);
    // $table->string('photo')->nullable();;
    // $table->string('nid')->nullable();

                      AppTextFieldWidget(labelText: 'Customer Name',prefixIcon: Icons.account_circle,controller:AddCustomerScreenController.to.customerNameTxt  ),
                      AppTextFieldWidget(labelText: 'Father Name',prefixIcon:Icons.account_box,controller:AddCustomerScreenController.to.customerFatherNameTxt),
                      AppTextFieldWidget(labelText: 'Email Address',prefixIcon: Icons.add_shopping_cart_sharp,controller:AddCustomerScreenController.to.customerEmailTxt ),
                      AppTextFieldWidget(labelText: ' Mobile Number',prefixIcon: Icons.call,controller:AddCustomerScreenController.to.customerPhoneNoTxt),

                    ],
                  ),
                ),  // all text field
                Container(
                    margin: EdgeInsets.only(left: 30,right: 20),
                    child: Text('Address',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600
                      ),)), // Address
                Container(
                  //width: Get.width,
                  margin: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                  padding: EdgeInsets.only(left: 10, right: 10,top: 10, bottom: 10),

                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.blue,width: 2),
                    borderRadius: BorderRadius.circular(8,),
                  ),
                  child: Column(
                    children:<Widget> [
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 5),
                        child: FormBuilderDropdown(
                          name: 'devision',
                          decoration: InputDecoration(
                            labelText: 'Devision Name',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),
                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Devision'),
                          onChanged: (value){
                           //  AddCustomerScreenController.to.divisionId = value.id;
                          },
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: divisions
                              .map((devision) => DropdownMenuItem(
                            value: devision,
                            child: Text('$devision'),
                          ))
                              .toList(),
                        ),
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 5),
                        child: FormBuilderDropdown(
                          name: 'district',
                          decoration: InputDecoration(
                            labelText: 'District Name',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),
                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select District'),
                          onChanged: (value){
                           // AddCustomerScreenController.to.districtId = value.id;
                           },
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: districts
                              .map((district) => DropdownMenuItem(
                            value: district,
                            child: Text('$district'),
                          )).toList(),
                        ),
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 5),
                        child: FormBuilderDropdown(
                          name: 'Upzilla',
                          decoration: InputDecoration(
                            labelText: 'Upazilla Name',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),
                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Upzilla'),
                          onChanged: (value){
                            // AddCustomerScreenController.to.upzillaId = value.id;
                          },
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: upazillas
                              .map((upazilla) => DropdownMenuItem(
                            value: upazilla,
                            child: Text('$upazilla'),
                          ))
                              .toList(),
                        ),
                      ),
                      Container(
                        margin:EdgeInsets.symmetric(vertical: 5),
                        child: FormBuilderDropdown(
                          name: 'Union/Counsilling',
                          decoration: InputDecoration(
                            labelText: 'Union/Counsilling Name',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue,width: 1)
                            ),
                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Union/Counsilling'),
                          onChanged: (value){
                            // AddCustomerScreenController.to.unionId = value.id;
                          },
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: upazillas
                              .map((upazilla) => DropdownMenuItem(
                            value: upazilla,
                            child: Text('$upazilla'),
                          ))
                              .toList(),
                        ),
                      ),

                      AppTextFieldWidget(labelText: 'Post Code',prefixIcon: Icons.local_post_office,controller: AddCustomerScreenController.to.postCodeTxt ),
                      AppTextFieldWidget(labelText: 'Road No',prefixIcon: Icons.add_road ,controller: AddCustomerScreenController.to.roadNoTxt),
                      AppTextFieldWidget(labelText: 'House No',prefixIcon: Icons.house ,controller: AddCustomerScreenController.to.houseNoTxt),
                      AppTextFieldWidget(labelText: 'Floor and Flat No',prefixIcon: Icons.house ,controller: AddCustomerScreenController.to.flatNoTxt),
                      //AppTextFieldWidget(labelText: 'floor',prefixIcon: Icons.house ),
                    ],
                  ),
                ),  // Address Details
                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 5,
                  ),
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: FormBuilderDropdown(
                    name: 'Select Profession',
                    decoration: InputDecoration(

                      labelStyle: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                      filled: true,
                      fillColor: Colors.grey[300],
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: Colors.blue, width: 1)),
                      focusColor: Colors.blue,
                    ),
                    allowClear: true,
                    hint: Text('Select Profession'),
                    onReset: (){
                     //AddCustomerScreenController.to.profesions = null;
                    },

                    onChanged: (value){
                      AddCustomerScreenController.to.selectProfesion = value;
                      print('selected value is ${value}');
                    },
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items:AddCustomerScreenController.to.profesions.map((e) => DropdownMenuItem(
                      value: e,
                      child: Text(e),
                    )).toList(),
                  ),
                ), // Select Profession

                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 5,
                  ),
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: FormBuilderDropdown(
                    name: 'SelectServiceType',
                    decoration: InputDecoration(
                      // labelText: 'Select Package',
                      labelStyle: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                      filled: true,
                      fillColor: Colors.grey[300],
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: Colors.blue, width: 1)),
                      focusColor: Colors.blue,
                    ),
                    // initialValue: 'Male',
                    allowClear: true,
                    hint: Text('Select Service Type'),
                    onReset: (){
                     // AddCustomerScreenController.to.aserviceType = null;
                    },

                    onChanged: (value){
                    //  AddCustomerScreenController.to.selectedServiceId = value.id;
                     // print('selected value is ${AddCustomerScreenController.to.aserviceType.serviceTypeName}');
                    },
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items:AddCustomerScreenController.to.serviceTypes.map((e) => DropdownMenuItem(
                      value: e,
                      child: Text(e.serviceTypeName),
                    )).toList(),
                  ),
                ), // Service Type

                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 7,
                  ),
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: FormBuilderDropdown(
                    name: 'SelectPackage',
                    decoration: InputDecoration(
                      // labelText: 'Select Package',
                      labelStyle: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                      filled: true,
                      fillColor: Colors.grey[300],
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: Colors.blue, width: 1)),
                      focusColor: Colors.blue,
                    ),
                    // initialValue: 'Male',
                    allowClear: true,
                    hint: Text('Select Package'),
                    onReset: (){
                      // AddCustomerScreenController.to.packageList = null;
                    },

                    onChanged: (value){
                       AddCustomerScreenController.to.selectedPackageId = value.packageId;
                      print('selected value is ${value.packageName}');
                    },
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items:AddCustomerScreenController.to.packageList.map<DropdownMenuItem<PackageModel>>((PackageModel value) {
                      return DropdownMenuItem<PackageModel>(
                        value: value,
                        child: Text(value.packageName),
                      );
                    }).toList(),


                  ),
                ), //  Package

                openDatePicker(context),

              //  packageDropdownMenu(context),
                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 7,
                  ),
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: FormBuilderDropdown(
                    name: 'SelectServiceArea',
                    decoration: InputDecoration(
                      // labelText: 'Select Package',
                      labelStyle: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                      filled: true,
                      fillColor: Colors.grey[300],
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: Colors.blue, width: 1)),
                      focusColor: Colors.blue,
                    ),
                    // initialValue: 'Male',
                    allowClear: true,
                    hint: Text('Select Service Area'),
                    onReset: (){
                    //  AddCustomerScreenController.to.aserviceType = null;
                    },

                    onChanged: (value){
                     // AddCustomerScreenController.to.selectedServiceId = value.id;
                     // print('selected value is ${value.serviceTypeName}');
                    },
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items:AddCustomerScreenController.to.serviceTypes.map((e) => DropdownMenuItem(
                      value: e,
                      child: Text(e.serviceTypeName),
                    )).toList(),
                  ),
                ), // Service Area
                Container(

                  padding: EdgeInsets.only(bottom: 10),
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blue,width: 2),
                      borderRadius: BorderRadius.circular(8,)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children:<Widget> [
                      Container(
                     width: 150,
                        alignment: Alignment.center,
                        padding: EdgeInsets.symmetric(horizontal: 5,vertical: 5),
                        child: FormBuilderImagePicker(name: 'photos',
                          decoration:  InputDecoration(labelText: 'Choose Photo',
                              labelStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black
                              )),
                          maxImages: 1,
                        ),
                      ),
                      Container(padding: EdgeInsets.only(top:20),
                          child: Container(width: 2,height: 100,color:Colors.blue[600])),
                      Container(
                       width: 150,
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 10,right: 10),
                        child: FormBuilderImagePicker(name: 'NID',
                          decoration: const InputDecoration(labelText: 'Choose NID Image',
                              labelStyle: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black
                              )),
                          maxImages: 1,
                        ),
                      ),
                    ],
                  ),
                ),//photo picker
                SizedBox(height:10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children:<Widget> [
                    /*
                    Container(
                      margin: EdgeInsets.fromLTRB(10,60,10,20),
                      child: RaisedButton.icon(
                        padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                        onPressed: (){
                          AddCustomerScreenController.to.saveNewCustomerInformation();
                        },
                        elevation: 10,
                        color: Colors.blue,
                        splashColor: Colors.red,
                        icon: Icon(Icons.assignment_turned_in_rounded,color: Colors.white,),

                        label: Text('SUBMIT',

                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 16,
                          ),

                        ),),),
                    */
                    AppButtonWidget(title: "SAVE",leadingCenter: true,width: 100,
                        height: 40,onTab: (){
                         AddCustomerScreenController.to.saveNewCustomerInformation();
                        }
                    ),
                  ],
                ), // Submit Button
                SizedBox(height: 300,)
              ],
            ),
          )
          ,
        ),
      ),
    );

  }



  packageDropdownMenu(BuildContext context) {
    return Container(
    //  margin: EdgeInsets.all(10),
      width: Get.width,
      height: 40,
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.appBarColor)

      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text('Case Type :   '),
          Expanded(
            child: Obx(() => DropdownButton<PackageModel>(

              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              elevation: 16,
              alignment: Alignment.center,
              style: const TextStyle(color: Colors.deepPurple),
              underline: SizedBox(),

              items:AddCustomerScreenController.to.packageList.map<DropdownMenuItem<PackageModel>>((PackageModel value) {
                return DropdownMenuItem<PackageModel>(
                  value: value,
                  child: Text(value.packageName),
                );
              }).toList(),
             ),
            ),
          )
        ],
      ),
    );
  }

  openDatePicker(BuildContext context){

    return Obx(() =>Container(
      margin: EdgeInsets.only(left: 20,right: 20),
      width: Get.width,
      height: 40,
      padding: EdgeInsets.only(left: 10,top: 10),
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.appBarColor)

      ),

      child: InkWell(

        child: Text(AddCustomerScreenController.to.selectedDate.value),
        onTap: (){
          _selectDate(context);
        },
      ),
    ));
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked.toString() != "Select Case Date"){
      print('okkk ====== ${picked}');
      String sldate = picked.toString();

      var inputFormat = formatDate(picked, [yyyy, '-', mm, '-', dd]);

      // formatDate('dd/MM/yyyy HH:mm');
      // var inputDate = inputFormat.parse('31/12/2000 23:59'); // <-- dd/MM 24H format
      //
      // var outputFormat = formatDate('MM/dd/yyyy hh:mm a');
      // var outputDate = outputFormat.format(inputDate);

      AddCustomerScreenController.to.selectedDate.value = inputFormat;


      print('okkk ====== ${AddCustomerScreenController.to.selectedDate.value}');
      //
    }

  }
}