
import 'package:ciss_app/data/Model/PackageModel.dart';
import 'package:ciss_app/data/Model/ServiceTypeModel.dart';
import 'package:ciss_app/presentation/Screen/PackageInfo/PackageScreenController.dart';
import 'package:ciss_app/presentation/widgets/AppButtonWidget.dart';
import 'package:ciss_app/presentation/widgets/AppColors.dart';
import 'package:ciss_app/presentation/widgets/AppTextFieldWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

class ServicePackageScreen extends StatelessWidget {

  final _globalFormKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {

    Get.lazyPut(() => PackageScreenController());
    PackageScreenController.to.getAllPackages();
    PackageScreenController.to.loadServiceTypeData();

    return Container(
      color: AppColors.bodyBackgroundColor,
      height: Get.height,
      width: Get.width,
      child: FormBuilder(
        key: _globalFormKey,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Text(
                    'Service Package information',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                      letterSpacing: .8,
                    ),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(
                          vertical: 10,
                        ),
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                        child: FormBuilderDropdown(
                          name: 'SelectPackage',
                          decoration: InputDecoration(
                           // labelText: 'Select Package',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                            filled: true,
                            fillColor: Colors.grey[300],
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.blue, width: 1)),
                            focusColor: Colors.blue,
                          ),
                          // initialValue: 'Male',
                          allowClear: true,
                          hint: Text('Select Package'),
                          onReset: (){
                            PackageScreenController.to.aserviceType = null;
                          },

                          onChanged: (value){
                           PackageScreenController.to.aserviceType = value;
                           print('selected value is ${PackageScreenController.to.aserviceType.serviceTypeName}');
                          },
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          // items: EmployeeOptions.map(
                          //     (EmployeeType) => DropdownMenuItem(
                          //           value: EmployeeType,
                          //           child: Text('$EmployeeType'),
                          //         )).toList(),
                          //
                          items:PackageScreenController.to.serviceTypes.map((e) => DropdownMenuItem(
                            value: e,
                            child: Text(e.serviceTypeName),
                          )).toList(),
                        ),
                      ),
                      AppTextFieldWidget(
                        controller: PackageScreenController.to.packageNameTxt,
                          hintText: 'Input Package Name',
                         // labelText: 'Package Name',
                          prefixIcon: Icons.account_circle),
                      AppTextFieldWidget(
                        controller: PackageScreenController.to.packageCodeTxt,
                          hintText: 'Input Package Code',
                         // labelText: 'Package Code',
                          prefixIcon: Icons.call),
                      AppTextFieldWidget(
                        controller: PackageScreenController.to.packageBandwidthTxt,
                          hintText: 'Input Bandwidth',
                         // labelText: 'Bandwidth',
                          prefixIcon: Icons.account_box),
                      AppTextFieldWidget(
                         controller: PackageScreenController.to.packagePriceTxt,
                          hintText: 'Input Package Price',
                         textInputType: TextInputType.number,
                         // labelText: 'Service Rate',
                          prefixIcon: Icons.account_box),
                      SizedBox(height: 10,),
                      AppButtonWidget(title: 'Save'
                          ,textAlign: TextAlign.center,font_size: 20,width: 150,height: 45,
                       onTab: (){
                        print('click ok');
                        PackageScreenController.to.savePackageInformation();
                       }
                      ),
                      Divider(
                        color: Colors.grey,
                      ),

                    ],
                  ),
                ),
                Center(child: Text( 'Available Service Packages')),
                SizedBox(
                  height: 10,
                ),
                Container(
                  color: Colors.grey[100],
                  width: Get.width,
                  height: 250,
                   child: _packageList(context),
                ),
              ],
            ),
          ),


        ),
      ),
    );
  }

  Widget _packageList(BuildContext context) {

    return  Obx((){
      if(PackageScreenController.to.packageList.value == null){
        return Container(
          height: 100,
          width: 100,
          child:CircularProgressIndicator(),
        );
      }else {
        return ListView.builder(
            itemCount: PackageScreenController.to.packageList.length,
            itemBuilder: (context, index) {
              PackageModel packageModel = PackageScreenController.to
                  .packageList[index];
              return ListTile(
                title: Text(''+packageModel.packageName + ', Bandwidth: ' +
                    packageModel.bandWidth + ', Price: ' + packageModel.price+'Tk'),
              );
            }
        );
      }
    });

  }


}