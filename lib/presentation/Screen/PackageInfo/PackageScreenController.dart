

import 'package:ciss_app/data/Model/PackageModel.dart';
import 'package:ciss_app/data/Model/ServiceTypeModel.dart';
import 'package:ciss_app/data/Repository/Error/failures.dart';
import 'package:ciss_app/data/Repository/Repo/PackageRepository.dart';
import 'package:ciss_app/data/Repository/core/AppAPIKey.dart';
import 'package:ciss_app/presentation/widgets/AppSnackBar.dart';
import 'package:dartz/dartz.dart';
import 'package:dartz/dartz_unsafe.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PackageScreenController extends GetxController{
  static PackageScreenController to = Get.find();
  PackageRepository repository = new PackageRepository();

  TextEditingController packageNameTxt = new TextEditingController();
  TextEditingController packageCodeTxt = new TextEditingController();
  TextEditingController packagePriceTxt = new TextEditingController();
  TextEditingController packageBandwidthTxt = new TextEditingController();
  TextEditingController packageTxt = new TextEditingController();

  RxList<PackageModel> packageList = <PackageModel>[].obs;
//  List<PackageModel> packageList = new List<PackageModel>();
  List<ServiceTypeModel> serviceTypes;
  ServiceTypeModel aserviceType = new ServiceTypeModel();


  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    setDemoData();
   // getAllPackages();


  }


  setDemoData(){
    packageNameTxt.text = 'demo ';
    packageCodeTxt.text = 'dcode';
    packagePriceTxt.text ='500';
    packageBandwidthTxt.text ='5mb';
    print('demo data set');
  }
  loadServiceTypeData(){
    serviceTypes  = List<ServiceTypeModel>();
    aserviceType.serviceTypeId = 1;
    aserviceType.serviceTypeName = 'Internet Service';
    serviceTypes.add(aserviceType);
    aserviceType = new ServiceTypeModel();
    aserviceType.serviceTypeId = 2;
    aserviceType.serviceTypeName = 'Dish Service';
    serviceTypes.add(aserviceType);
    aserviceType = null;// new ServiceTypeModel();
   // aserviceType.serviceTypeId = 0;
  }
  savePackageInformation(){

    if(formDataValidation()){
    PackageModel packageModel = new PackageModel();

    packageModel.packageName  = packageNameTxt.text;
    packageModel.packageCode = packageCodeTxt.text;
    packageModel.price = packagePriceTxt.text;
    packageModel.bandWidth = packageBandwidthTxt.text;
    ServiceTypeModel serviceTypeModel = new ServiceTypeModel();
    serviceTypeModel.serviceTypeId = '1';
    packageModel.serviceTypeModel = serviceTypeModel;

    print(' calling ====='+packageModel.packageName + packageModel.packageCode + packageModel.price);
    PackageRepository.saveNewPackageInformation(packageModel);
    getAllPackages();
    }
  }


  getAllPackages() async {

    Either<dynamic, Failure> response = await PackageRepository.getServicePackageInformation();

    response.fold((l) {

      if(l== null)
        return;
      print("Left ===>  ${l}");
      if (l[AppAPIKey.success].toString() == 'true' && l[AppAPIKey.status_code].toString() == '200' ) {

        List<PackageModel> lst = (l['data'] as List).map((itemWord) => PackageModel.fromJson(itemWord)).toList();
        packageList.value = lst;
        print('all completed orders ${packageList}');
       }else {
       // AppSnackBar.errorSnackbar(msg: l['message']);
      }
    }, (r) {
      print("Right ${r}");
    });


  }

  // getAllPackages() async{
  //    List<PackageModel> lst  = await PackageRepository.getServicePackageInformation();
  //   print('Service Type loaded');
  //   forEach(lst, am){
  //     print(am.packageName);
  //   }
  //   packageList.value = lst;
  //   packageList.refresh();
  //
  // }


 bool formDataValidation(){
    if(aserviceType == null){
      AppSnackBar.errorSnackbar(msg: 'Please Select Service Type');
      return false;
    }
    // else if(aserviceType.serviceTypeId == 0) {
    //   AppSnackBar.errorSnackbar(msg: 'Please Select Service Type 1');
    //   return false;
    // }
    else if(packageNameTxt.text.isEmpty) {
      AppSnackBar.errorSnackbar(msg: 'Package Name is Required');
      return false;
    }
    else if(packageCodeTxt.text.isEmpty) {
      AppSnackBar.errorSnackbar(msg: 'Package Code is Required');
      return false;
    }
    else if(packageBandwidthTxt.text.isEmpty) {
      AppSnackBar.errorSnackbar(msg: 'Package Bandwidth is Required');
      return false;
    }
    else if(packagePriceTxt.text.isEmpty) {
      AppSnackBar.errorSnackbar(msg: 'Package Name is Required');
      return false;
    }
    return true;
  }
}
